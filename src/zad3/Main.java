package zad3;

public class Main {
    public static void main(String[] args) {
        System.out.println(" ".isBlank()); //true
        System.out.println(" ".isEmpty()); //false

        "AB\nAB\nAB".lines().forEach(System.out::println);
        //AB
        //AB
        //AB

        System.out.println("DB ".strip()); // 'DB'

        System.out.println(" DB ".stripLeading()); // 'DB '

        System.out.println(" DB ".stripTrailing()); // ' DB'

        System.out.println("==".repeat(5)); // ==========
    }
}
