package zad1;

import java.util.List;

public interface MyInterface {

    private List<Integer> privateMethod() {
        return List.of(-50, -10, 1, 2, 3, 4, 5, 6, 7);
    }

    default void printDefault() {
        privateMethod().stream().filter(integer -> integer > 0).findFirst().ifPresent(System.out::println);
    }

}
