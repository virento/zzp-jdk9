package zad4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Main {
    public static void main(String[] args) throws IOException {
        long pos = Files.mismatch(Path.of("file1.txt"), Path.of("file2.txt"));
        System.out.println(pos); //19

        long pos2 = Files.mismatch(Path.of("file1.txt"), Path.of("file1-identyczny.txt"));
        System.out.println(pos2); //-1

        System.out.println(Files.readString(Path.of("file1.txt")));
        System.out.println(Files.readString(Path.of("file1-identyczny.txt")));
        System.out.println(Files.readString(Path.of("file2.txt")));
    }
}
