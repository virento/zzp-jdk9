package zad2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        // 1
        var integerList = new ArrayList<>(Arrays.asList(-1, 1, 2, 3, 4, 5, 6, 7, 8));

        //2
        var unmodifiableList = List.copyOf(integerList);

        //3
        List<Integer> list = integerList.stream().filter(integer -> integer >= 0).collect(Collectors.toUnmodifiableList());

        //4 - Error is thrown
        list.add(1);

    }
}
